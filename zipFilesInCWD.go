package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func askForConfirmation(s string) bool {
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Printf("%s [y/n]", s)
		response, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		response = strings.ToLower(strings.TrimSpace(response))
		if response == "y" || response == "yes" {
			return true
		} else if response == "n" || response == "no" {
			return false
		}
	}
}

//TODO
func listFiles(cwd string) {
	err := filepath.Walk(cwd, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if filepath.Ext(path) == ".exe" {
			fmt.Printf("visited file: %q modified: %q\n", path, info.ModTime())
			//TODO
			//zip file
		}
		return nil
	})

	if err != nil {
		fmt.Printf("error walking the path %q: %v\n", cwd, err)
	}
}

//TODO
func zipFile(file string) {

}

func main() {
	cwd, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	c := askForConfirmation(fmt.Sprintf("After this operation all files in folder %s will be archived and source files wil be deleted", cwd))
	if c {
		listFiles(cwd)
		fmt.Println("Operation completed")
		return
	}
}
